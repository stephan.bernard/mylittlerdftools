import re
import sys
import json
import getopt
import requests



# --------------------------------------------------
# --------------------- CONFIG ---------------------
# --------------------------------------------------
PREFIXES_FILE = sys.path[0]+"/prefixes.ttl"
BASE_ELEMENT = ""
SPARQL_ENDPOINT = ""
CSS_URL = ""

# ----------------------------------
# ---------- sparql_query ----------
# ----------------------------------
def sparql_query(server, sparql_query, silent=False):
    headers = {'Accept': 'application/json'}
    query = {'query': sparql_query}

    req = requests.get(server,  params=query, headers=headers)
    if (req.status_code != requests.codes.ok):
        if not silent:
            sys.stderr.write("\n=SPARQL ERROR===> [%d] :\n%s\n" %
                             (req.status_code, query['query']))
        return None
    s = json.loads(req.text)
    return s['results']['bindings']


# --------------------------------------------
# ---------- get prefixes from file ----------
# --------------------------------------------
# Returns a dictionnary {'prefix':'uri'},
# e.g { 'dct': 'http://purl.org/dc/terms/', ... }
# Returns None if file not found or file is None.
def load_prefixes(file, write_errors=True):
    find_prefix = re.compile(r'@?prefix\s+(\S+):\s*<([\S]+)>\s*\.?',
                             flags=re.I)
    prefixes = {'terms':'http://purl.org/dc/terms/',
                'rdfs':'http://www.w3.org/2000/01/rdf-schema#',
                'skos':'http://www.w3.org/2004/02/skos/core#',
                'owl':'http://www.w3.org/2002/07/owl#',}
    if file is None:
        return None
    f = open(file, "r")
    lines = f.readlines()
    for line in lines:
        pr = find_prefix.findall(line.strip())
        for e in pr:
            if prefixes.get(e[0]) is None:
                prefixes[e[0]] = e[1]
            elif write_errors and (prefixes[e[0]] != e[1]):
                sys.stderr.write("ATTENTION: préfixe %s: défini 2× :\n" % e[0])
                sys.stderr.write("    <%s> et <%s>.\n" % (prefixes[e[0]], e[1]))
                sys.stderr.write("    <%s> sera utilisé.\n\n" % prefixes[e[0]])
    return prefixes

# --------------------------------------------------
# -------------------- Prefixed --------------------
# --------------------------------------------------
# If uri is "http://…/Foo" will return 'ex:Foo'
# if ex is in prefixes. Will return unchanged uri
# if no prefix is found.
def prefixed(uri, prefixes):
  for p in prefixes:
    if (uri.startswith(prefixes[p])) \
       and (len(uri)>len(prefixes[p])): # To avoid only "prefix:"
      return p+":"+uri[len(prefixes[p]):]
  return '&lt;%s&gt;' % uri
  
# --------------------------------------------------
# ------------- Extract string litteral ------------
# --------------------------------------------------
litteral_nb = 0
# Returns '<tag><span lang="…">value</span></tag>'
# for every data[…][key]. If there is no xml:lang
# in the results, will skip <span />.
def get_string_litteral(data, key, tag, classe=None):
  global litteral_nb
  if data is None: return ''
  ret = ' '
  for ti in data:
    closeret = ''
    if classe is not None:
      ret += ('<%s class="%s">\n' % (tag, classe))
    else:
      ret += ('<%s>\n' % tag)
    for langtag in ['xml:lang', 'lang']:
        if (ti[key].get(langtag) is not None):
            ret += '  <span id="langsel" lang="%s"' % ti[key][langtag]
            ret += ' onclick="tc(\'%s_%d\')"' % (ti[key][langtag], litteral_nb)
            ret += '>['+ti[key][langtag]+']</span>'
            ret += '<span id="%s_%d"' % (ti[key][langtag], litteral_nb)
            ret += ' lang="%s" class="txt"' % ti[key][langtag]
            ret += ' vis="' # Visibility: 'n':normal, 'l':lighter
            if (ti[key][langtag]=='fr'): ret += 'n'
            else: ret += 'l'
            ret += '">\n'
            litteral_nb += 1
            closeret = '</span>' + closeret
    ret += ti[key]['value']
    ret += closeret+(' </%s>\n' % tag)
  return ret


# --------------------------------------------------
# --------------- Get element content --------------
# --------------------------------------------------
def get_uri_link(obj):
    if obj['type'] != 'uri': return ''
    ret = '<a class="hval" href="'+obj['value']
    ret += '">%s</a>' % obj['_nom_']
    return ret

def get_litteral(obj):
    global litteral_nb
    closeret = ''
    ret = ''
    for langtag in ['xml:lang', 'lang']:
        if (obj.get(langtag) is not None):
            ret += '   <span id="langsel" lang="%s"' % obj[langtag]
            ret += ' onclick="tc(\'%s_%d\')"' % (obj[langtag], litteral_nb)
            ret += '>['+obj[langtag]+']</span>'
            ret += '<span id="%s_%d"' % (obj[langtag], litteral_nb)
            ret += ' lang="%s" class="txt"' % obj[langtag]
            ret += ' vis="' # Visibility: 'n':normal, 'l':lighter
            if (obj[langtag]=='fr'): ret += 'n'
            else: ret += 'l'
            ret += '">\n'
            litteral_nb += 1
            closeret = '</span>' + closeret
    ret += obj['value']
    ret += '%s' % closeret
    return ret

def get_element_content(data, key_property, key_object, key_list, prefixes):
  if (data is None) or (len(data) == 0):
    return ''
  ret = ' <table class="content"><tbody>\n'
  
  # To make access easier
  kp = key_property
  ko = key_object

  # Replace prefixes and list properties
  prop = []
  href = []
  for d in data:
    for k in [kp, ko, key_list]:
      if (d.get(k) is not None):
          if d[k]['type'] == 'uri':
              d[k]['_nom_'] = prefixed(d[k]['value'], prefixes)
    if (d[kp].get('_nom_') is not None) \
       and (d[kp]['_nom_'] not in prop):
      prop.append(d[kp]['_nom_'])
      href.append(d[kp]['value'])
    if d[kp].get('_nom_') is None:
      d[kp]['_nom_'] = '_noname_'
      
  # Make table
  for p,h in zip(prop,href):
    ret += '  <tr><td><a class="hprop" href="%s">%s</a></td>\n   <td>\n' % (h,p)
    closeret = ''
    for d in data:
      if d[kp]['_nom_'] == p:
        if d[ko]['type'] == 'uri':
          ret += '  <div class="value">'
          ret += get_uri_link(d[ko])
          ret += '</div>\n'
        elif (d[ko]['type'] == 'bnode') \
             and (d.get(key_list) is not None): # a list
          if closeret == '':
              closeret = ' )</div>'
              ret += '  <div class="value">('
          if d[key_list]['type'] == 'uri':
              ret += ' '+get_uri_link(d[key_list])
          else:
              ret += ' '+get_litteral(d[key_list])
        else:
          ret += '  <div class="value">\n'
          ret += get_litteral(d[ko])
          ret += '\n  </div>'
    ret+='%s\n  </td></tr>\n' % closeret

  ret += ' </tbody></table>\n'
  return ret

# --------------------------------------------------
# -------------- Get elements of type --------------
# --------------------------------------------------
# Will query to find elements of type ty
# (ex : owl:AnnotationProperty) to display them.
# If any, it'll also add the title, but won't if none.
# WARNING : is ty is a URI, write it with '<' and '>'.
def get_elements_of_type(title, ty, base, prfx):
    prfx_str=''
    for p in prfx:
        prfx_str += "PREFIX "+p+": <"+prfx[p]+">\n"

    query = """SELECT DISTINCT ?e WHERE { ?e a %s
} ORDER BY ?e""" % ty
    data = sparql_query(SPARQL_ENDPOINT, prfx_str+query)
    elts = [d['e'] for d in data if d['e']['value'].startswith(base)]
    if len(elts) == 0:
        return '',''
    ret = '<h2>'+title+'</h2>\n'
    mnu = ' <li id="mtitle">'+title+'<li>\n'
    for d in elts:
        ret += '<h3 id="%s">' % d['value'][len(base)+1:]
        ret += prefixed(d['value'], prfx)+'</h3>\n'
        mnu += ' <li id="melt"><a href="#%s">' % d['value'][len(base)+1:]
        mnu += prefixed(d['value'], prfx).split('/')[-1].split(':')[-1]
        mnu += '</a></li>\n'
        # --- Title
        query = """SELECT ?t WHERE {
{<%s> terms:title ?t} UNION {<%s> rdfs:label ?t}
} ORDER BY lang(?t)""" % (d['value'], d['value'])
        titles = sparql_query(SPARQL_ENDPOINT, prfx_str+query)
        ret += get_string_litteral(titles, "t", "h4")
        # --- Definition
        query = """SELECT ?t WHERE {<%s> skos:definition ?t}
ORDER BY lang(?t)""" % d['value']
        desc = sparql_query(SPARQL_ENDPOINT, prfx_str+query)
        if (desc is not None) and (len(desc) > 0):
            ret += ' <div id="definition">\n'
            ret += get_string_litteral(desc, "t", "p", "def_text")
            ret += ' </div>\n'
        # --- Description
        query = """SELECT ?t WHERE {<%s> terms:description ?t
} ORDER BY lang(?t)""" % d['value']
        desc = sparql_query(SPARQL_ENDPOINT, prfx_str+query)
        if (desc is not None) and (len(desc) > 0):
            ret += ' <div id="description">'
            ret += '<div id="lbl">Description :</div>\n'
            ret += get_string_litteral(desc, "t", "p", "desc_text")
            ret += ' </div>\n'
        # --- And others
        query = """SELECT ?p ?o ?l WHERE {<%s> ?p ?o
OPTIONAL { ?o rdf:rest*/rdf:first ?l }
FILTER (?p NOT IN (terms:description, skos:definition,
        rdfs:label, terms:title))
} ORDER BY ?p lang(?o)""" % d['value']
        oth = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
        ret += get_element_content(oth, "p", "o", "l", prfx)
    return ret,mnu
    
# --------------------------------------------------
# --------------------- Usage ----------------------
# --------------------------------------------------
def usage():
    ret = "Usage : python %s [options] <URI>" % sys.argv[0]
    ret += """
  Prints on standard output the html code to be
  displayed when dereferencing a owl:Ontology with
  a web browser.
  URI is the main element to be documented
  (should instanciate a owl:Ontology).

Options :
  -p <file.ttl>/--prefix=<file.ttl> :
    Gets the prefixes from file.ttl.
    This file should be in turtle format, ex :
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    …
    If none given, will use the default prefix file used
    for <https://opendata.inrae.fr/fcu-def> ontology
    (for wich this program is initially made for).
  -e <endpoint>/--endpoint=<endpoint> :
    The endpoint used to retrieve information
    about the ontology.
    If not given, will use <URI>/sparql.
  -c <URL>/--css=<URL> :
    The URL of css file to use to display this
    html content.
    If none given, will use <URI>/style.css.
"""
    return ret
    
    

# --------------------------------------------------
# ---------------------- MAIN ----------------------
# --------------------------------------------------

# ------------- Commandline parameters -------------
opts, args = getopt.getopt(sys.argv[1:], 'p:e:c:h', [
    'prefix=', 'endpoint=', 'css=', 'help',
])

try:
    if (('-h','') in opts) or (('--help','') in opts):
        print(usage())
        sys.exit(0)
    BASE_ELEMENT = args[0]
    SPARQL_ENDPOINT = BASE_ELEMENT + "/sparql"
    CSS_URL = BASE_ELEMENT + "/style.css"
    for o in opts:
        if (o[0] == '-p') or (o[0] == '--prefix'):
            PREFIXES_FILE = o[1]
        elif (o[0] == '-e') or (o[0] == '--endpoint'):
            SPARQL_ENDPOINT = o[1]
        elif (o[0] == '-c') or (o[0] == '--css'):
            CSS_URL = o[1]
except Exception as exc:
    print(exc, file=sys.stderr)
    print("\n\n", file=sys.stderr)
    print(usage(), file=sys.stderr)
    sys.exit(-1)

# -------------------- Let's go --------------------
prefixes = load_prefixes(PREFIXES_FILE)
# Par sécurité :
prefixes_str=''
for p in prefixes:
  prefixes[p] = prefixes[p].strip()
  prefixes_str += "PREFIX "+p+": <"+prefixes[p]+">\n"


html = '<!doctype html>\n'
html += '<html lang="fr">\n'
html += '<head>\n'
html += ' <meta charset="utf-8" />\n'
html += ' <title>'+BASE_ELEMENT+'</title>\n'
html += f' <link href="{CSS_URL}" rel="stylesheet" type="text/css" />\n'
html += '</head><body onload="chgLang()">\n'
html += '<script>\n'
jvs  = 'function tc(id){' # tc: Toggle(visible on)Click.
jvs += 'var x = document.getElementById(id);'
jvs += 'if (x.getAttribute("vis")=="n") {x.setAttribute("vis","l");'
jvs += '} else {x.setAttribute("vis","n");}}'
jvs += 'function chgLang(){'
jvs += 'var l = document.getElementById("lsel").value;'
jvs += 'var xx = document.getElementsByClassName("txt");'
jvs += 'for (i=0;i<xx.length;i++) if(xx[i].hasAttribute("lang")){'
jvs += 'if((xx[i].lang == l)||(l == "all"))'
jvs += ' xx[i].setAttribute("vis","n");'
jvs += 'else xx[i].setAttribute("vis","l");'
jvs += '}}'
html += jvs+'\n</script>'
header = html
html = '<div id="content">\n'

# Menu :
menu = '<div id="menu">\n'

# ========== languages

query = """SELECT DISTINCT ?la WHERE { [] ?p ?l; 
BIND (lang(?l) AS ?la)} ORDER BY ?la"""
ll = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
if (ll is not None) and (len(ll) > 0):
    lang = [l['la']['value'] for l in ll if l.get('la') is not None]
    lang.remove('')
    menu += '<div id="lang_sel"><label for="lsel">lang:</label>\n'
    menu += ' <select id="lsel" name="lsel" onChange="chgLang()">\n'
    menu += ' <option value="all" selected="true">all</option>\n'
    for l in lang:
        menu+=' <option value="%s">%s</option>\n' % (l,l)
    menu += '</select></div>\n'

menu += '<ul><li id="mhome"><a href="#home">Home</a></li>\n'


# ========== main element

# --- Titre
html += '<h1 id="home">'+BASE_ELEMENT+'</h1>\n'
query = """SELECT ?t WHERE { <%s> terms:title ?t
} ORDER BY lang(?t)""" % BASE_ELEMENT
data = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
if (data is not None) and (len(data) > 0):
  html += get_string_litteral(data, "t", "h4")

# --- Definition
query = """SELECT ?t WHERE {<%s> skos:definition ?t}
ORDER BY lang(?t)""" % BASE_ELEMENT
desc = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
if (len(desc) > 0):
    html += ' <div id="definition">'
    html += get_string_litteral(desc, "t", "p", "def_text")
    html += '</div>\n'

# --- Description
query = """SELECT ?t WHERE {<%s> terms:description ?t
} ORDER BY lang(?t)""" % BASE_ELEMENT
desc = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
if (desc is not None) and (len(desc) > 0):
    html += ' <div id="description">'
    html += '<div id="lbl">Description :</div>'
    html += get_string_litteral(desc, "t", "p", "desc_text")
    html += '</div>\n'

# --- Le reste
query = """SELECT ?p ?o ?l WHERE {<%s> ?p ?o
OPTIONAL { ?o rdf:rest*/rdf:first ?l }
FILTER ((?p != terms:description) && (?p != terms:title))
}
ORDER BY ?p lang(?o)""" % BASE_ELEMENT
data = sparql_query(SPARQL_ENDPOINT, prefixes_str+query)
html += get_element_content(data, "p", "o", "l", prefixes)

# ========== Annotation Properties
htemp,mtemp = get_elements_of_type('Annotation properties',
                             'owl:AnnotationProperty',
                             BASE_ELEMENT, prefixes)
html += htemp
menu += mtemp

# ========== Object Properties
htemp,mtemp = get_elements_of_type('Object properties',
                             'owl:ObjectProperty',
                             BASE_ELEMENT, prefixes)
html += htemp
menu += mtemp

# ========== Data Properties
htemp,mtemp = get_elements_of_type('Data properties',
                             'owl:DatatypeProperty',
                             BASE_ELEMENT, prefixes)
html += htemp
menu += mtemp

# ========== Classes
htemp,mtemp = get_elements_of_type('Classes',
                             'owl:Class',
                             BASE_ELEMENT, prefixes)
html += htemp
menu += mtemp

# ========== Individuals
htemp,mtemp = get_elements_of_type('Individuals',
                             'owl:NamedIndividual',
                             BASE_ELEMENT, prefixes)
html += htemp
menu += mtemp

menu += '</ul></div>'
html += '</div></body></html>'
#print(BeautifulSoup(header+menu+html, 'html.parser').prettify())
print(header+menu+html)

sys.exit(0)

