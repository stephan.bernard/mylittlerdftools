# Static dereferencing tools

## owl2html

A little tool used to create an html page about an owl:Ontology.

Example usage :

```sh
python src/owl2html.py -p src/prefixes.ttl -e <triplestore_address> \
  -c /style.css <base_uri> > index.html
```

### Remarks

- RDF lists are processed, but I don't guarantee the order
  of the elements
- Blank nodes are not processed.
